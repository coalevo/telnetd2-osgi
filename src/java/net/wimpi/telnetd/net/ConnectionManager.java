/***
 * Java TelnetD library (embeddable telnet daemon)
 * Copyright (c) Dieter Wimberger
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the author nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS
 * IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ***/
package net.wimpi.telnetd.net;

import net.wimpi.telnetd.service.ConnectionFilter;
import net.wimpi.telnetd.util.DictionaryUtility;
import net.wimpi.telnetd.impl.Activator;
import net.wimpi.telnetd.net.ssh.SecureSocket;
import org.apache.commons.collections.list.CursorableLinkedList;
import org.osgi.service.cm.ConfigurationException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.Stack;

/**
 * Manages the connections of the {@link net.wimpi.telnetd.service.TelnetListenerService}
 * instance it is associated with.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public class ConnectionManager
    implements Runnable {

  private Thread m_Thread;
  private ThreadGroup m_ThreadGroup;  //ThreadGroup all connections run in
  private CursorableLinkedList m_OpenConnections;
  private Stack m_ClosedConnections;
  private ConnectionFilter m_Filter; //reference to the connection filter
  private int m_MaxConnections;      //maximum allowed connections stored from the properties
  private long m_WarningTimeout;    //time to idle warning
  private int m_DisconnectTimeout;    //time to idle diconnection
  private int m_HousekeepingInterval;  //interval for managing cleanups
  private String m_StartShell;
  private boolean m_LineMode = false;
  private boolean m_Stopping = false;
  private boolean m_Started = false;


  public ConnectionManager() {
  }//constructor

  /**
   * Set a connection filter for this
   * ConnectionManager instance. The filter is used to handle
   * IP level allow/deny of incoming connections.
   *
   * @param filter ConnectionFilter instance.
   */
  public void setConnectionFilter(ConnectionFilter filter) {
    m_Filter = filter;
  }//setConnectionFilter

  /**
   * Gets the active ConnectionFilter instance or
   * returns null if no filter is set.
   *
   * @return the managers ConnectionFilter.
   */
  public ConnectionFilter getConnectionFilter() {
    return m_Filter;
  }//getConnectionFilter

  /**
   * Returns the number of open connections.
   *
   * @return the number of open connections as <tt>int</tt>.
   */
  public int openConnectionCount() {
    return m_OpenConnections.size();
  }//openConnectionCount

  /**
   * Returns the {@link Connection} at the given index.
   *
   * @param idx an index as <tt>int</tt>.
   * @return a {@link Connection} instance.
   */
  public Connection getConnection(int idx) {
    return (Connection) m_OpenConnections.get(idx);
  }//getConnection

  /**
   * Get all {@link Connection} instances with the given
   * <tt>InetAddress</tt>.
   *
   * @param addr the {@link Connection} instances by address.
   * @return all {@link Connection} instances with the given
   *         <tt>InetAddress</tt>.
   */
  public Connection[] getConnectionsByAdddress(InetAddress addr) {
    ArrayList l = new ArrayList();
    for (Iterator iterator = m_OpenConnections.listIterator(); iterator.hasNext();) {
      Connection connection = (Connection) iterator.next();
      if (connection.getConnectionData().getInetAddress().equals(addr)) {
        l.add(connection);
      }
    }
    Connection[] conns = new Connection[l.size()];
    return (Connection[]) l.toArray(conns);
  }//getConnectionsByAddress

  /**
   * Starts this <tt>ConnectionManager</tt>.
   */
  public void start() {
    Activator.getServices().debug("start()::" + this.toString());
    m_ThreadGroup = new ThreadGroup(new StringBuffer().append(this.toString()).append("Connections").toString());
    m_OpenConnections = new CursorableLinkedList();
    m_ClosedConnections = new Stack();
    m_Thread = new Thread(this);
    m_Thread.start();
    m_Started = true;
  }//start

  /**
   * Stops this <tt>ConnectionManager</tt>.
   */
  public void stop() {
    Activator.getServices().debug("stop()::" + this.toString());
    if (m_Started) {
      m_Started = false;
      m_Stopping = true;
      //wait for thread to die
      try {
        m_Thread.interrupt();
        m_Thread.join();
      } catch (InterruptedException iex) {
        Activator.getServices().error("stop()", iex);
      }
      for (Iterator iter = m_OpenConnections.iterator(); iter.hasNext();) {
        try {
          Connection tc = (Connection) iter.next();
          //maybe write a disgrace to the socket?
          tc.close();
        } catch (Exception exc) {
          Activator.getServices().error("stop()", exc);
        }
      }
      Activator.getServices().debug("stop():: Stopped " + this.toString());

    }
    m_ClosedConnections.clear();
    m_OpenConnections.clear();
    m_ClosedConnections = null;
    m_OpenConnections = null;
    m_StartShell = null;
    m_Filter = null;
    m_Thread = null;
    m_ThreadGroup = null;
  }//stop

  /**
   * Method that that tries to connect an incoming request.
   * Properly  queueing.
   *
   * @param insock Socket thats representing the incoming connection.
   */
  public void makeConnection(Socket insock) {
    if (!m_Started) {
      return;
    }
    Activator.getServices().debug("makeConnection()::" + insock.toString());
    if (m_Filter == null ||
        (m_Filter != null && m_Filter.isAllowed(insock.getInetAddress()))) {
      //we create the connection data object at this point to
      //store certain information there.
      ConnectionData newCD = new ConnectionData(insock, this);
      newCD.setStartShell(m_StartShell);
      newCD.setLineMode(m_LineMode);
      if (m_OpenConnections.size() < m_MaxConnections) {
        //create a new Connection instance
        Connection con = new Connection(newCD);
        Thread t = new Thread(m_ThreadGroup, con);
        con.setThread(t);
        //log the newly created connection
        Object[] args = {new Integer(m_OpenConnections.size() + 1)};
        Activator.getServices().info(MessageFormat.format("connection #{0,number,integer} made.", args));
        //register it for being managed
        m_OpenConnections.add(con);
        //start it
        t.start();
      }
    } else {
      Activator.getServices().info("makeConnection():: Active Filter blocked incoming connection.");
      try {
        insock.close();
      } catch (IOException ex) {
        //do nothing or Activator.getServices().
      }
    }
  }//makeConnection


  /**
   * Periodically does following work:
   * <ul>
   * <li> cleaning up died connections.
   * <li> checking managed connections if they are working properly.
   * </ul>
   */
  public void run() {
    //housekeep connections
    try {
      do {
        //clean up closed connections
        cleanupClosed();
        //check all active connections
        checkOpenConnections();
        //sleep interval
        try {
          Thread.sleep(m_HousekeepingInterval);
        } catch (InterruptedException ex) {
          if (!m_Stopping) {
            Activator.getServices().error("run", ex);
          }
        }
      } while (!m_Stopping);

    } catch (Exception e) {
      Activator.getServices().error("run()", e);
    }
    Activator.getServices().debug("run():: Ran out " + this.toString());
  }//run

  private void cleanupClosed() {
    if (m_Stopping) {
      return;
    }
    //cleanup loop
    while (!m_ClosedConnections.isEmpty()) {
      Connection nextOne = (Connection) m_ClosedConnections.pop();
      Activator.getServices().info("cleanupClosed():: Removing closed connection " + nextOne.toString());
      m_OpenConnections.remove(nextOne);
    }
  }//cleanupBroken

  private void checkOpenConnections() {
    if (m_Stopping) {
      return;
    }
    //do routine checks on active connections
    for (Iterator iter = m_OpenConnections.listIterator(); iter.hasNext();) {
      Connection conn = (Connection) iter.next();
      ConnectionData cd = conn.getConnectionData();
      //check if it is dead and remove it.
      if (!conn.isActive()) {
        registerClosedConnection(conn);
        continue;
      }
      /* Timeouts check */
      //first we caculate the inactivity time
      long inactivity = System.currentTimeMillis() - cd.getLastActivity();
      //now we check for warning and disconnection
      if (inactivity > m_WarningTimeout) {
        //..and for disconnect
        if (inactivity > (m_DisconnectTimeout + m_WarningTimeout)) {
          //this connection needs to be disconnected :)
          Activator.getServices().debug("checkOpenConnections():" + conn.toString() + " exceeded total timeout.");
          //fire logoff event for shell site cleanup , beware could hog the daemon thread
          conn.processConnectionEvent(new ConnectionEvent(ConnectionEvent.CONNECTION_TIMEDOUT));
        } else {
          //this connection needs to be warned :)
          if (!cd.isWarned()) {
            Activator.getServices().debug("checkOpenConnections():" + conn.toString() + " exceeded warning timeout.");
            cd.setWarned(true);
            //warning event is fired but beware this could hog the daemon thread!!
            conn.processConnectionEvent(new ConnectionEvent(ConnectionEvent.CONNECTION_IDLE));
          }
        }
      }
      /* end Timeouts check */
    }
  }//checkConnections

  public void registerClosedConnection(Connection con) {
    if (m_Stopping) {
      return;
    }
    if (!m_ClosedConnections.contains(con)) {
      Activator.getServices().debug("registerClosedConnection()::" + con.toString());
      m_ClosedConnections.push(con);
    }
  }//unregister

  public void configure(Dictionary conf)
      throws ConfigurationException {

    m_MaxConnections = DictionaryUtility.getInt(conf, "connection.max");
    m_WarningTimeout = DictionaryUtility.getLong(conf, "connection.idle.warning");
    m_DisconnectTimeout = DictionaryUtility.getInt(conf, "connection.idle.logout");
    m_HousekeepingInterval = DictionaryUtility.getInt(conf, "connection.housekeeping.interval");
    m_StartShell = DictionaryUtility.getString(conf, "connection.startshell");
    String inputmode = DictionaryUtility.getString(conf, "connection.inputmode", VALID_MODES);
    m_LineMode = false;
    if (inputmode.equalsIgnoreCase("line")) {
      m_LineMode = true;
    }
  }//configure

  private static final String[] VALID_MODES = {"line", "character"};

}//class ConnectionManager