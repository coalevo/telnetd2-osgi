/***
 * Java TelnetD library (embeddable telnet daemon)
 * Copyright (c) Dieter Wimberger
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the author nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS
 * IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ***/
package net.wimpi.telnetd.shell;

import bsh.Interpreter;
import bsh.NameSpace;
import net.wimpi.telnetd.io.TerminalOutputStream;
import net.wimpi.telnetd.io.TerminalPrintStream;
import net.wimpi.telnetd.io.TerminalReader;
import net.wimpi.telnetd.net.Connection;
import net.wimpi.telnetd.net.ConnectionEvent;
import net.wimpi.telnetd.impl.Activator;

/**
 */
public class BeanScriptShell
    extends AbstractBaseShell {

  protected Interpreter m_Interpreter;
  protected boolean m_Exit = false;

  /**
   * constructor and init methods
   */
  public BeanScriptShell() {
  }//constructor

  public void run(Connection con) {
    //1. prepare
    prepare(con);
    m_Interpreter = new Interpreter(new TerminalReader(m_TermIO),
        new TerminalPrintStream(new TerminalOutputStream(m_TermIO)),
        new TerminalPrintStream(new TerminalOutputStream(m_TermIO)),
        true);

    m_Interpreter.setClassLoader(Thread.currentThread().getContextClassLoader());

    try {
      initializeInterpreter();
      //run
      m_Interpreter.run();
    } catch (Exception ex) {
      Activator.getServices().error("run():", ex);
    }
    con.removeConnectionListener(this);
    return;
  }//run

  /**
   * Method that can be overridden to initialize the
   * Interpreter or it's namespace with package and
   * class imports, as well as variables required.
   */
  protected void initializeInterpreter() {
    //imports
    NameSpace ns = m_Interpreter.getNameSpace();
    ns.importPackage("net.wimpi.telnetd.io.terminal");
    //variables
    try {
      m_Interpreter.set("termio", m_TermIO);
      m_Interpreter.set("cdata", m_ConData);
      m_Interpreter.set("env", m_Environment);
    } catch (Exception ex) {
      Activator.getServices().error("initializeInterpreter():", ex);
    }
  }//initializeInterpreter

  //ConnectionListener Implementation
  public void connectionIdle(ConnectionEvent ce) {
    execute(new LogoutHandler("connectionIdle()", "connection_event_idle_warning"));
  }//connectionIdle

  public void connectionTimedOut(ConnectionEvent ce) {
    execute(new LogoutHandler("connectionTimedOut()", "connection_event_idle_loggedout"));
  }//connectionTimedOut

  public void connectionLogoutRequest(ConnectionEvent ce) {
    execute(new LogoutHandler("connectionLogoutRequest()", "connection_event_logoutrequest"));
  }//connectionLogoutRequest

  public void connectionSentBreak(ConnectionEvent ce) {
    Activator.getServices().debug("connectionSentBreak");
  }//connectionSentBreak

  class LogoutHandler implements Runnable {

    protected String m_Debug;
    protected String m_Reason;

    public LogoutHandler(String debug, String reason) {
      m_Debug = debug;
      m_Reason = reason;
    }//constructor

    public void run() {
      Activator.getServices().debug(m_Debug);
      try {
        m_TermIO.write(m_Reason);
      } catch (Exception ex) {
        Activator.getServices().error("run()", ex);
      }
      forceTelnetClose();
    }//run

  }//inner class LogoutHandler

}//BeanScriptShell

